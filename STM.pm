package STM;

use strict;
use warnings;
use Fcntl;
use SDBM_File;
use Carp;
use Data::Dumper;
use Cwd;
use Storable;
use Algorithm::Porter_Stemmer_mod;
use Statistics::PCA_mod;
use AI::NeuralNet::SOM;
use AI::NeuralNet::SOM::Rect;
use WordNet::QueryData;

#-----------------------CONSTRUCTORS

sub new {
	my ($class, %args) = @_;
	bless {
		_concept_doc_vecs	=> undef,
		_concept_query_vecs	=> undef,
		_concept_doc_vecs_db => $args{concept_doc_vecs_db} || "",
		_concept_query_vecs_db	=> $args{concept_query_vecs_db} || "",
		_corpus_directory	=>	$args{corpus_directory}	|| "",
		_corpus_vocab_db	=>	$args{corpus_vocab_db}	|| "",
		_corpus_vocab_done	=>	0,
		_defined_no_of_retrieval	=>	4,
		_defined_vocab_size	=>	5500,
		_defined_vocab_concept_size => 4000,
		_doc_hist_template	=>	{},
		_idf_filter_option	=>	exists $args{idf_filter_option} ? $args{idf_filter_option} : 1,
		_idf_t				=> 	undef,
		_idf_t_db			=>	$args{idf_t_db} || "",
		_min_word_length	=>	$args{min_word_length}	|| 4,
		#_normalized_doc_vecs =>	{},
		_normalized_doc_vecs_without_term => undef,
		#_normalized_doc_vecs_db =>  $args{normalized_doc_vecs_db} || "normalized_doc_vecs_db",
		_normalized_doc_vecs_without_term_db	=>	$args{normalized_doc_vecs_without_term_db} || "normalized_doc_vecs_without_term_db",
		_normalized_query_vecs =>	{},
		_normalized_query_vecs_without_term => {},
		_normalized_query_vecs_db	=>	"normalized_query_vecs_db",
		_normalized_query_vecs_without_term_db		=> "normalized_query_vecs_without_term_db",
		_pca_concept_doc_vecs	=> undef,
		_pca_concept_query_vecs	=> undef,
		_pca_concept_doc_vecs_db	=> $args{pca_concept_doc_vecs_db}	|| "",
		_pca_concept_query_vecs_db	=> $args{pca_concept_query_vecs_db}	|| "",
		_pca_concept_features	=> undef,
		_pca_concept_features_file	=> $args{pca_concept_features_file} || "",
		_pca_term_doc_vecs		=>	undef,
		_pca_term_doc_vecs_db		=>	$args{pca_term_doc_vecs_db} || "",
		_pca_term_features		=>	undef,
		_pca_term_features_file	=>	$args{pca_term_features_file} || "",
		_pca_term_query_vecs	=> undef,
		_system_performance	=> undef,
		_ranked_retrieval_association => undef,
		_ranked_retrieval_association_db => $args{ranked_retrieval_association_db} || "",
		_SOM				=> 	undef,
		_SOM_db				=>	$args{SOM_db} || "",
		_stop_words			=>	[],
		_stop_words_file	=>	$args{stop_words_file}	|| "",
		_query_dir			=>	$args{query_directory} || "Query",
		_total_num_of_docs	=>	0,
		_vocab_concept		=> undef,
		_vocab_concept_db	=> $args{vocab_concept_db} || "",
		_vocab_hist_on_disk =>	{},
		_vocab_hist			=>	{},
		_vocab_idf_hist		=>	{},
		_vocab_size			=>	undef,
		_want_stemming		=>	$args{want_stemming}	|| 0,
		_term_significance	=> 1,
		_concept_significance	=> 0,
		_working_directory  =>  cwd,
	}, $class;
}

#-----------------------METHODS FOR VOCABULARY & HISTOGRAMS CONSTRUCTION
####### Directory Scanner
sub _scan_directory {
	print "<debug>I'm in _scan_directory method...\n";
	my $self = shift;
	my $dir = shift;
	chdir $dir or die "Unable to change directory to $dir: $!";
	$dir = cwd;
	foreach (glob "*") {
		if(-d and !(-l)) {
			$self->_scan_directory($_);
			chdir $dir or die "Unable to change directory to $dir: $!";
		} elsif(-r _ and -T _ and -M _ > 0.00001 and !(-l $_) and !m{\.ps$} and !m{\.pdf$} and !m{\.eps$} and !m{\.out$} and !m{~$}) {
			$self->_scan_file($_) unless $self->{_corpus_vocab_done};
			$self->_construct_doc_vector($_) if $self->{_corpus_vocab_done};
		}
	}
}

sub _scan_file {
	print "<debug>I'm in _scan_file method...\n";
	my $self = shift;
	my $file = shift;
	open IN, $file;
	my $min = $self->{_min_word_length};
	my %uniques = ();
	while(<IN>) {
		chomp;
		my @brokeup = split /\"|\'|\.|\(|\)|\[|\]|\\|\/|\s+/, $_;
		my @clean_words = grep $_, map {/([a-z0-9_]{$min,})/i;$1} @brokeup;
		next unless @clean_words;
		@clean_words = grep $_, Algorithm::Porter_Stemmer_mod::stem(@clean_words) if $self->{_want_stemming};
		map {$self->{_vocab_hist_on_disk}->{"\L$_"}++} grep $_, @clean_words;
		for (@clean_words) { $uniques{"\L$_"}++ };
	}
	close(IN);
	map {$self->{_vocab_idf_hist}->{"\L$_"}++} keys %uniques;
	$self->{_total_num_of_docs}++;
}

########## Stop words Removal
sub _drop_stop_words {
	my $self = shift;
	open(IN, "$self->{_working_directory}/$self->{_stop_words_file}")
		or die "unable to open stop words file: $!";
	
	while (<IN>) {
		next if /^#/;
		next if /^[ ]*$/;
		chomp;
		delete $self->{_vocab_hist_on_disk}->{$_}
			if exists $self->{_vocab_hist_on_disk}->{$_};
		delete $self->{_vocab_idf_hist}->{$_}
			if exists $self->{_vocab_idf_hist}->{$_};
		unshift @{$self->{_stop_words}}, $_;
	}
}

########## Construct Corpus Vocabulary and save on provided file name
sub construct_corpus_vocabulary {
	my $self = shift;
	#print Dumper($self), "\n";
	die "You must supply the name of the corpus directory to the constructor" unless $self->{_corpus_directory};
	print "<debug>Scanning the directory '$self->{_corpus_directory}' for vocabulary construction\n";
	
	unlink glob "$self->{_corpus_vocab_db}.*";
	
	tie %{$self->{_vocab_hist_on_disk}}, 'SDBM_File', $self->{_corpus_vocab_db}, O_RDWR|O_CREAT, 0666
		or die "Can't create DBM files: $!";
		
	$self->_scan_directory($self->{_corpus_directory});
	$self->_drop_stop_words() if $self->{_stop_words_file};
	delete $self->{_stop_words};
	
	#Calculate idf(t);
	foreach(keys %{$self->{_vocab_idf_hist}}) {
		$self->{_idf_t}->{$_} = abs( (1 + log($self->{_total_num_of_docs}
                                      /
                                     (1 + $self->{_vocab_idf_hist}->{$_}))) 
                                   / log(10) );
	}
	
	#Calculate tf-idf(t);
	foreach(keys %{$self->{_vocab_hist_on_disk}}) {
		$self->{_vocab_hist_on_disk}->{$_} = $self->{_vocab_hist_on_disk}->{$_} * $self->{_idf_t}->{$_};
	}
	delete $self->{_vocab_idf_hist};

	my $term_count = 0;
	foreach(reverse sort { $self->{_vocab_hist_on_disk}->{$a} <=> $self->{_vocab_hist_on_disk}->{$b} } keys %{$self->{_vocab_hist_on_disk}}) {
		$self->{_vocab_hist}->{$_} = $self->{_vocab_hist_on_disk}->{$_};
	}
	
	print Dumper(scalar(keys $self->{_vocab_hist_on_disk}));
	
	#BEGIN Construct the Concept Vocabulary
	$term_count = 1;
	my $concept_string = "";
	my $synset;
	my $term_temp;
	
	while(scalar(keys $self->{_vocab_hist}) > 0) {
		foreach(reverse sort { $self->{_vocab_hist}->{$a} <=> $self->{_vocab_hist}->{$b} } keys %{$self->{_vocab_hist}}) {
			my @concept_bag;
			
			next if(!$self->{_vocab_hist}->{$_});
			
			delete $self->{_vocab_hist}->{$_};
			$synset = $self->getSynsWordNet($_);
			
			if($synset) {
				push @concept_bag, $_;
				
				foreach $term_temp (@{$synset}) {
					if($self->{_vocab_hist}->{$term_temp}) {
						push @concept_bag, $term_temp;
						delete $self->{_vocab_hist}->{$term_temp};
					}
				}
				
				$self->{_vocab_concept}->{$concept_string . $term_count} = \@concept_bag;
				#print $_,"\n";print Dumper($self->{_vocab_concept}->{$concept_string . $term_count});
				$term_count++;
			} else {
				push @concept_bag, $_;
				$self->{_vocab_concept}->{$concept_string . $term_count} = \@concept_bag;
				#print $_,"\n";print Dumper($self->{_vocab_concept}->{$concept_string . $term_count});
				$term_count++;
			}
		}
	}
	
#	print Dumper($self->{_vocab_concept});
	#END Construct the Concept Vocabulary
	
	$term_count = 0;
	foreach(reverse sort { $self->{_vocab_hist_on_disk}->{$a} <=> $self->{_vocab_hist_on_disk}->{$b} } keys %{$self->{_vocab_hist_on_disk}}) {
		$self->{_vocab_hist}->{$_} = $self->{_vocab_hist_on_disk}->{$_};
#		$term_count++;
#		last if $term_count == $self->{_defined_vocab_size};
	}
	
#	print Dumper($self->{_vocab_hist});
	
	untie %{$self->{_vocab_hist_on_disk}};
	
	$self->{_corpus_vocab_done} = 1;
	$self->{_vocab_size} = scalar(keys %{$self->{_vocab_hist}});
	
	#Storage idf(t) for later query normalizing usage
	chdir $self->{_working_directory};
	eval {
        store($self->{_idf_t}, $self->{_idf_t_db});
        store($self->{_vocab_concept}, $self->{_vocab_concept_db});
    };
    if ($@) {
        print "Something wrong with disk storage of idf(t): $@";
    }
}

########## Display the corpus vocabulary
sub display_corpus_vocab {
	my $self = shift;
	my $para = shift;
	
	if($para eq "term") {
		die "corpus vocabulary not yet constructed" unless keys %{$self->{_vocab_hist}};
		print "\n\n Displaying corpus vocabulary:\n\n";
		
		foreach( reverse sort { $self->{_vocab_hist}->{$a} <=> $self->{_vocab_hist}->{$b} } keys %{$self->{_vocab_hist}}) {
			my $outstring = sprintf("%30s    %f", $_, $self->{_vocab_hist}->{$_});
			print "$outstring\n";
		}
		
		my $vocab_size = scalar(keys %{$self->{_vocab_hist}});
		print "\n Size of the corpus vocabulary: $vocab_size\n\n";
	} elsif ($para eq "concept") {
		$self->{_vocab_concept} = retrieve($self->{_vocab_concept_db}) unless $self->{_vocab_concept};
		foreach(sort {$a <=> $b} keys $self->{_vocab_concept}) {
			print $_, " => ";
			print Dumper($self->{_vocab_concept}->{$_});
		}
		
		my $vocab_size = scalar(keys $self->{_vocab_concept});
		print "\n Size of the concept vocabulary: $vocab_size\n\n";
	} else {
		die "Unrecognized Parameter(s)!!!!";
	}
}

########## Upload a Constructed Model
sub upload_stm_model_from_disk {
	my $self = shift;
	die "\n Cannot find the database files for the STM model"
		unless -s "$self->{_corpus_vocab_db}.pag";
		#&& -s $self->{_normalized_doc_vecs_without_term_db};
		
	#$self->{_normalized_doc_vecs} = retrieve($self->{_normalized_doc_vecs_db});
	#$self->{_normalized_doc_vecs_without_term} = retrieve($self->{_normalized_doc_vecs_without_term_db}) unless $self->{_normalized_doc_vecs_without_term};
	#$self->{_pca_doc_vecs} = retrieve($self->{_pca_doc_vecs_db});
	#$self->{_SOM} = retrieve($self->{_SOM_db});
	
	tie %{$self->{_vocab_hist_on_disk}}, 'SDBM_File', $self->{_corpus_vocab_db}, O_RDONLY, 0666
		or die "Can't open DBM file: $!";
	
	my $term_count = 0;
	foreach(reverse sort { $self->{_vocab_hist_on_disk}->{$a} <=> $self->{_vocab_hist_on_disk}->{$b} } keys %{$self->{_vocab_hist_on_disk}}) {
		$self->{_vocab_hist}->{$_} = $self->{_vocab_hist_on_disk}->{$_};
#		$term_count++;
#		last if $term_count == $self->{_defined_vocab_size};
	}
	$self->{_corpus_vocab_done} = 1;
	
	untie %{$self->{_vocab_hist_on_disk}};
}

########## Generate Document Vectors
sub generate_document_vectors {
	my $self = shift;
	
	chdir $self->{_working_directory};
	
	$self->{_idf_t} = retrieve($self->{_idf_t_db}) unless $self->{_idf_t};
	$self->{_vocab_concept} = retrieve($self->{_vocab_concept_db}) unless $self->{_vocab_concept};
	
	foreach( reverse sort { $self->{_vocab_hist}->{$a} <=> $self->{_vocab_hist}->{$b} } keys %{$self->{_vocab_hist}}) {
		$self->{_doc_hist_template}->{$_} = 0;
	}
	
	tie %{$self->{_vocab_hist_on_disk}}, 'SDBM_File', $self->{_corpus_vocab_db}, O_RDONLY, 0666
		or die "Can't open DBM file: $!";
	
	my $term_count = 0;
	$self->{_vocab_hist} = undef;
	foreach(reverse sort { $self->{_vocab_hist_on_disk}->{$a} <=> $self->{_vocab_hist_on_disk}->{$b} } keys %{$self->{_vocab_hist_on_disk}}) {
		$self->{_vocab_hist}->{$_} = $self->{_vocab_hist_on_disk}->{$_};
		$term_count++;
		last if $term_count == $self->{_defined_vocab_size};
	}
	$self->{_corpus_vocab_done} = 1;
	
	untie %{$self->{_vocab_hist_on_disk}};
	
	$self->_scan_directory($self->{_corpus_directory});
	
	chdir $self->{_working_directory};
=pod
    eval {
        store( $self->{_corpus_doc_vectors}, $self->{_doc_vectors_db} );
    };
    if ($@) {
        print "Something went wrong with disk storage of document vectors: $@";
    }
=cut

    eval {
        #store($self->{_normalized_doc_vecs}, $self->{_normalized_doc_vecs_db});
        print "\n<DEBUG> Saving normalized doc vecs to disk...\n";
        store($self->{_normalized_doc_vecs_without_term}, $self->{_normalized_doc_vecs_without_term_db});
        print "\n<DEBUG> Saving normalized doc vecs to disk... DONE\n";
        
        print "\n<DEBUG> Saving concept doc vecs to disk...\n";
        store($self->{_concept_doc_vecs}, $self->{_concept_doc_vecs_db});
        print "\n<DEBUG> Saving concept doc vecs to disk... DONE\n";
    };
    if ($@) {
        print "Something wrong with disk storage of normalized doc vecs: $@";
    }
}

########## Construct the Document Vector for a Document
sub _construct_doc_vector {
	my $self = shift;
	my $file = shift;
	print "<Debug>Scanning $file to construct its document vector\n";
	
	my %document_vector = %{deep_copy_hash($self->{_doc_hist_template})};
	#foreach ( sort keys %{$self->{_doc_hist_template}} ) {  
        #$document_vector{$_} = 0;    
    #}
    
    my $min = $self->{_min_word_length};
    my $total_words_in_doc = 0;
    unless (open IN, $file) {
        print "Unable to open file $file in the corpus: $!\n";
        return;
    }
    while (<IN>) {
        chomp;                                                    
        my @brokenup = split /\"|\'|\.|\(|\)|\[|\]|\\|\/|\s+/, $_;
        my @clean_words = grep $_, map { /([a-z0-9_]{$min,})/i;$1 } @brokenup;
        next unless @clean_words;
        @clean_words = grep $_, Algorithm::Porter_Stemmer_mod::stem(@clean_words) if $self->{_want_stemming};
        map { $document_vector{"\L$_"}++ } 
                grep {exists $self->{_doc_hist_template}->{"\L$_"}} @clean_words; 
    }
    close IN;
    #die "Something went wrong. Doc vector size unequal to vocab size"
        #unless $self->{_defined_vocab_size} == scalar(keys %document_vector);

    foreach (keys %document_vector) {        
        $total_words_in_doc += $document_vector{$_};
    }
    
    my %normalized_doc_vec;
    if ($self->{_idf_filter_option}) {
        foreach (keys %document_vector) {        
            $normalized_doc_vec{$_} = $document_vector{$_}
                                      *
                                      $self->{_idf_t}->{$_}
                                      /
                                      ($total_words_in_doc + 1);
        }
    }
    
    #BEGIN construct concept vector <== HERE
#    print Dumper(\%normalized_doc_vec);
    
    my %concept_doc_vec;
    my $concept_temp;
    my $term_temp;
    foreach $concept_temp (sort {$a <=> $b} keys $self->{_vocab_concept}) {
    	$concept_doc_vec{$concept_temp} = 0;
    	foreach $term_temp (@{$self->{_vocab_concept}->{$concept_temp}}) {
    		$concept_doc_vec{$concept_temp} += $normalized_doc_vec{$term_temp};
    	}
    }
    
#    print Dumper(\%concept_doc_vec);
    #END contruct concept vector
    
    #BEGIN Remove term and concept to generate TERM_HISTOGRAMS & CONCEPT_HISTOGRAMS
    my @normalized_doc_vec_without_term;
    my @concept_doc_vec_without_concept;
    my $iCount = 0;
    
    foreach(sort keys $self->{_vocab_hist}){
    	push(@normalized_doc_vec_without_term, $normalized_doc_vec{$_});
    	$iCount++;
    	last if $iCount == $self->{_defined_vocab_size};
    }
    
    $iCount = 0;
    foreach(sort {$a <=> $b} keys $self->{_vocab_concept}) {
    	push @concept_doc_vec_without_concept, $concept_doc_vec{$_};
    	$iCount++;
    	last if $iCount == $self->{_defined_vocab_concept_size};
    }
    
#    print Dumper(\@normalized_doc_vec_without_term);
#    print Dumper(\@concept_doc_vec_without_concept);
    #END Remove term and concept to generate TERM_HISTOGRAMS & CONCEPT_HISTOGRAMS
    
    my $pwd = cwd;
    $pwd =~ m{$self->{_corpus_directory}.?(\S*)$};
    my $file_path_name;
    unless ( $1 eq "" ) {
        $file_path_name = "$1/$file";
    } else {
        $file_path_name = $file;
    }
    #$self->{_corpus_doc_vectors}->{$file_path_name} = \%document_vector;
    #$self->{_normalized_doc_vecs}->{$file_path_name} = \%normalized_doc_vec;
    $self->{_normalized_doc_vecs_without_term}->{$file_path_name} = \@normalized_doc_vec_without_term;
    $self->{_concept_doc_vecs}->{$file_path_name} = \@concept_doc_vec_without_concept;
}

# Meant only for an un-nested hash:
sub deep_copy_hash {
    my $ref_in = shift;
    my $ref_out = {};
    foreach ( keys %{$ref_in} ) {
        $ref_out->{$_} = $ref_in->{$_};
    }
    return $ref_out;
}

############# Display normalized document vectors
sub display_normalized_doc_vectors {
    my $self = shift;
    die "normalized document vectors not yet constructed" 
        unless keys %{$self->{_normalized_doc_vecs_without_term}};
        
    unless ($self->{_idf_filter_option}) {
        print "Nothing to display for normalized doc vectors since you did not set the use_idf_filter option in the constructor\n";
        return;
    }
    
=pod
    foreach my $file (sort keys %{$self->{_normalized_doc_vecs}}) {        
        print "\n\ndisplay normalized doc vec for $file:\n";
        foreach ( sort keys %{$self->{_normalized_doc_vecs}->{$file}} ) {
            print "$_  =>   $self->{_normalized_doc_vecs}->{$file}->{$_}\n";
        }
        my $docvec_size = keys %{$self->{_normalized_doc_vecs}->{$file}};
        print "\nSize of normalized vector for $file: $docvec_size\n";
    }
=cut
    
    foreach my $file (keys %{$self->{_normalized_doc_vecs_without_term}}) {
    	print "\n\ndisplay normalized doc vec without term for $file:\n";
    	print Dumper($self->{_normalized_doc_vecs_without_term}->{$file});
    }
}

sub dump_normalized_doc_vectors {
	my $self = shift;
	
	$self->{_normalized_doc_vecs_without_term} = retrieve($self->{_normalized_doc_vecs_without_term_db}) unless $self->{_normalized_doc_vecs_without_term};
	
	die "normalized document vectors not yet constructed" 
        unless keys %{$self->{_normalized_doc_vecs_without_term}};
        
    unless ($self->{_idf_filter_option}) {
        print "Nothing to display for normalized doc vectors since you did not set the use_idf_filter option in the constructor\n";
        return;
    }
    
    print Dumper($self->{_normalized_doc_vecs_without_term});
    
    $self->{_concept_doc_vecs} = retrieve($self->{_concept_doc_vecs_db}) unless $self->{_concept_doc_vecs};
    
    die "concept document vectors not yet constructed" 
        unless keys %{$self->{_concept_doc_vecs}};
    
    print Dumper($self->{_concept_doc_vecs});
}

#-----------------------METHODS FOR PCA COMPRESSION
############# Create & Store the PCA features including Averages & Projection Matrix
sub PCAProjectionMatrixConstruction {
	print "\n<DEBUG> Performing PCA projection...\n";
	
	my $self = shift;
	chdir $self->{_working_directory};
	
	my $pca = Statistics::PCA_mod->new;
	
	$pca->{pca_features} = retrieve($self->{_pca_features_file}) if -s $self->{_pca_features_file};
	
	#prepare data for PCA
	print "\n\t<DEBUG> Loading Normalized Doc Vecs...\n";
	$self->{_normalized_doc_vecs_without_term} = retrieve($self->{_normalized_doc_vecs_without_term_db}) unless $self->{_normalized_doc_vecs_without_term};
	print "\n\t<DEBUG> Loading Normalized Doc Vecs...DONE\n";
		
	if(!$pca->{pca_features}) {
		print "\n\t<DEBUG> No PCA Features stored. Performing fresh computation...\n";
		
		my $data = [];
		foreach(sort keys $self->{_normalized_doc_vecs_without_term}) {
			push(@$data, $self->{_normalized_doc_vecs_without_term}->{$_});
		}
		
		$pca->load_data({
			format	=>	'table',
			data	=>	$data,
		});
	} else {
		print "\n\t<DEBUG> Loaded  stored PCA Features. Performing transformation ONLY...\n";
	}
	
	#Perform PCA analysis
	$self->{_pca_features} = $pca->pca();
	
	my $pca_processed_matrix = delete $self->{_pca_features}->{pca_processed_matrix};

	my $iCount = 0;
	foreach(sort keys $self->{_normalized_doc_vecs_without_term}) {
		$self->{_pca_doc_vecs}{$_} = $pca_processed_matrix->[0][$iCount];
		$iCount++;
	}
	
	if($iCount ne scalar(keys $self->{_normalized_doc_vecs_without_term})) {
		print "<Debug> Something wrong with mapping the pca vectors back to corresponding docs: iCount = $iCount, scalar = " , scalar($self->{_normalized_doc_vecs_without_term}), "\n\n";
	}
	
	eval {
		print "\n\t<DEBUG> Storing PCA features...\n";
		store($self->{_pca_features}, $self->{_pca_features_file});
		print "\n\t<DEBUG> Storing PCA features...DONE\n";
		print "\n\t<DEBUG> Storing PCA Doc Vecs...\n";
		store($self->{_pca_doc_vecs}, $self->{_pca_doc_vecs_db});
		print "\n\t<DEBUG> Storing PCA Doc Vecs...DONE\n";
	};
	if ($@) {
		print "\n\n <Debug> Something wrong with the storage of PCA features";
	}
	
	#print the result
	$pca->results();
	#print Dumper($self->{_pca_features});
}

#-----------------------METHODS FOR SOM CLUSTERING
sub SOMClustering {
	my $self = shift;
	chdir $self->{_working_directory};
	
	$self->{_pca_term_doc_vecs} = retrieve($self->{_pca_term_doc_vecs_db}) unless $self->{_pca_term_doc_vecs};
	$self->{_pca_concept_doc_vecs} = retrieve($self->{_pca_concept_doc_vecs_db}) unless $self->{_pca_concept_doc_vecs};

	# Prepare data for SOM
	my $data_vecs = [];
	my $data_docs = [];
	foreach(sort keys $self->{_pca_term_doc_vecs}) {
		my $array_temp = [];
		
		push($data_docs, $_);
		push $array_temp, (@{$self->{_pca_term_doc_vecs}->{$_}}, @{$self->{_pca_concept_doc_vecs}->{$_}});
		push($data_vecs, $array_temp);
		
		$array_temp = ();
	}
	
	#FREE unused Resources
	$self->{_pca_term_doc_vecs} = ();
	$self->{_pca_concept_doc_vecs} = ();
	
	# Perform SOM Clustering of doc data
	$self->{_SOM} = new AI::NeuralNet::SOM::Rect(
		output_dim => "6x8", 
		input_dim => $#{$data_vecs->[0]} + 1,
		pca_term_length => 45,
		pca_concept_length => 45,
		term_significance	=> $self->{_term_significance},
		concept_significance	=> $self->{_concept_significance}
	);
	
	$self->{_SOM}->initialize;
	$self->{_SOM}->train(100, $data_vecs, $data_docs);

	eval {
		store($self->{_SOM}, $self->{_SOM_db});
	};
	if ($@) {
		print "\n\n <Debug> Something wrong with the storage of SOM";
	}
	
	#print Dumper($self->{_SOM});
	#print Dumper($self->{_SOM}->{docs_map_association});
}

#-----------------------METHODS FOR DOCUMENT RETRIEVAL
############## Main retrieval function
sub docs_retrieval {
	print "\n<DEBUG> In STM's docs_retrieval for DOCUMENT RETRIEVAL\n";
	
	my $self = shift;
	my $para = shift;
	
	$self->{_idf_t} = retrieve($self->{_idf_t_db}) unless $self->{_idf_t};
	
	# Step 1 - Generate the normalized vector for query documents
	$self->_generate_query_vector();
	
	$self->{_vocab_hist} = (); $self->{_vocab_concept} = ();
	$self->{_doc_hist_template} = (); $self->{_idf_t} = ();
	
	if($para eq "VSM") {
		$self->VSM_docs_retrieval();
		
		return;
	}
	
	# Step 2 - Compress the queries' normalized vectors
	$self->_PCA_compression_on_query_vecs();
	
	if($para eq "PCA") {
		$self->PCA_docs_retrieval();
		
		return;
	}
	
	# Step 3 - Perform SOM-based retrieval
	$self->_SOM_based_retrieval("default");
	
	# Step 4 - Perform Ranking on retrieved documents based on distance between queries and candidates
	$self->_retrieval_result_ranking("default");
	
	#TODO Step 5 - Perform Precision, Recall & F-Measure Calculation based on files of relevancy
	
}

############# Document retrieval based on PCA feature only
sub PCA_docs_retrieval {
	print "\n\t\t<DEBUG> In STM's PCA docs_retrieval...\n";
	
	my $self = shift;
	
	#print Dumper($self->{_pca_term_query_vecs});
	$self->{_pca_concept_query_vecs} = ();
	
	$self->{_pca_term_doc_vecs} = retrieve($self->{_pca_term_doc_vecs_db}) unless $self->{_pca_term_doc_vecs};
	
	my $query; my $candidate; my $distance; my $dist_doc_assoc;
	
	for $query (keys $self->{_pca_term_query_vecs}) {
		$dist_doc_assoc = [];
		
		for $candidate (keys $self->{_pca_term_doc_vecs}) {
			$distance = AI::NeuralNet::SOM::Utils::vector_distance($self->{_pca_term_query_vecs}->{$query}, $self->{_pca_term_doc_vecs}->{$candidate});
			push $dist_doc_assoc, [$candidate, $distance];
		}
		@{$dist_doc_assoc} = sort {$b->[1] <=> $a->[1]} @{$dist_doc_assoc};
		
		my @temp;
		
		for (my $i = 0; $i < 4; $i++) {
			push @temp, pop @{$dist_doc_assoc}; 
		}
		
		$self->{_ranked_retrieval_association}->{$query} = \@temp;
		
		$dist_doc_assoc = ();
	}
	
	eval {
        store( $self->{_ranked_retrieval_association}, $self->{_ranked_retrieval_association_db} );
    };
    if ($@) {
        print "Something went wrong with disk storage of document vectors: $@";
    }
	
	print Dumper($self->{_ranked_retrieval_association});
}

############# Document retrieval based on VSM feature only
sub VSM_docs_retrieval {
	print "\n\t\t<DEBUG> In STM's VSM docs_retrieval...\n";
	
	my $self = shift;
	
	chdir $self->{_working_directory};
	
	$self->{_concept_query_vecs} = ();
	$self->{_normalized_doc_vecs_without_term} = retrieve($self->{_normalized_doc_vecs_without_term_db}) unless $self->{_normalized_doc_vecs_without_term};
	
	my $query; my $candidate; my $distance; my $dist_doc_assoc;
	
	for $query (keys $self->{_normalized_query_vecs_without_term}) {
		$dist_doc_assoc = [];
		
		for $candidate (keys $self->{_normalized_doc_vecs_without_term}) {
			$distance = AI::NeuralNet::SOM::Utils::vector_distance($self->{_normalized_query_vecs_without_term}->{$query}, $self->{_normalized_doc_vecs_without_term}->{$candidate});
			push $dist_doc_assoc, [$candidate, $distance];
		}
		@{$dist_doc_assoc} = sort {$b->[1] <=> $a->[1]} @{$dist_doc_assoc};
		
		my @temp;
		
		for (my $i = 0; $i < 4; $i++) {
			push @temp, pop @{$dist_doc_assoc}; 
		}
		
		$self->{_ranked_retrieval_association}->{$query} = \@temp;
		
		$dist_doc_assoc = ();
	}
	
	eval {
        store( $self->{_ranked_retrieval_association}, $self->{_ranked_retrieval_association_db} );
    };
    if ($@) {
        print "Something went wrong with disk storage of document vectors: $@";
    }
	
	print Dumper($self->{_ranked_retrieval_association});
}

############# Query document vector construction
sub _generate_query_vector {
	print "\n\t<DEBUG> 1st generate DOC VECS for QUERIES...\n";
	
	my $self = shift;
	
	chdir $self->{_working_directory};
	
	$self->{_vocab_concept} = retrieve($self->{_vocab_concept_db}) unless $self->{_vocab_concept};
	
	foreach( reverse sort { $self->{_vocab_hist}->{$a} <=> $self->{_vocab_hist}->{$b} } keys %{$self->{_vocab_hist}}) {
		$self->{_doc_hist_template}->{$_} = 0;
	}
	
	$self->{_vocab_hist} = ();
	
	tie %{$self->{_vocab_hist_on_disk}}, 'SDBM_File', $self->{_corpus_vocab_db}, O_RDONLY, 0666
		or die "Can't open DBM file: $!";
	
	my $term_count = 0;
	$self->{_vocab_hist} = undef;
	foreach(reverse sort { $self->{_vocab_hist_on_disk}->{$a} <=> $self->{_vocab_hist_on_disk}->{$b} } keys %{$self->{_vocab_hist_on_disk}}) {
		$self->{_vocab_hist}->{$_} = $self->{_vocab_hist_on_disk}->{$_};
		$term_count++;
		last if $term_count == $self->{_defined_vocab_size};
	}
	
	untie %{$self->{_vocab_hist_on_disk}};#print Dumper($self->{_vocab_hist});die;
	
	$self->_scan_query_directory($self->{_query_dir});
#	print Dumper($self->{_normalized_query_vecs_without_term});print Dumper($self->{_concept_query_vecs});die;
	
#	chdir $self->{_working_directory};
#	
#    eval {
#        #store($self->{_normalized_query_vecs}, $self->{_normalized_query_vecs_db});
#        store($self->{_normalized_query_vecs_without_term}, $self->{_normalized_query_vecs_without_term_db});
#    };
#    if ($@) {
#        print "Something wrong with disk storage of normalized query vecs: $@";
#    }
}

sub _scan_query_directory {
	my $self = shift;
	my $dir = shift;
	chdir $dir or die "Unable to change directory to $dir: $!";
	$dir = cwd;
	foreach (glob "*") {
		if(-d and !(-l)) {
			$self->_scan_query_directory($_);
			chdir $dir or die "Unable to change directory to $dir: $!";
		} elsif(-r _ and -T _ and -M _ > 0.00001 and !(-l $_) and !m{\.ps$} and !m{\.pdf$} and !m{\.eps$} and !m{\.out$} and !m{~$}) {
			die "There is no Vocabulary of the Corpus" unless $self->{_corpus_vocab_done};
			$self->_construct_query_vector($_) if $self->{_corpus_vocab_done};
		}
	}
}

sub _construct_query_vector {
	my $self = shift;
	my $file = shift;
	
	my %document_vector = %{deep_copy_hash($self->{_doc_hist_template})};
    
    my $min = $self->{_min_word_length};
    my $total_words_in_doc = 0;
    unless (open IN, $file) {
        print "Unable to open file $file in the corpus: $!\n";
        return;
    }
    while (<IN>) {
        chomp;                                                    
        my @brokenup = split /\"|\'|\.|\(|\)|\[|\]|\\|\/|\s+/, $_;
        my @clean_words = grep $_, map { /([a-z0-9_]{$min,})/i;$1 } @brokenup;
        next unless @clean_words;
        @clean_words = grep $_, Algorithm::Porter_Stemmer_mod::stem(@clean_words) if $self->{_want_stemming};
        map { $document_vector{"\L$_"}++ } 
                grep {exists $self->{_doc_hist_template}->{"\L$_"}} @clean_words; 
    }
    close IN;
    #die "Something went wrong. Doc vector size unequal to vocab size"
        #unless $self->{_defined_vocab_size} == scalar(keys %document_vector);

    foreach (keys %document_vector) {        
        $total_words_in_doc += $document_vector{$_};
    }
    
    my %normalized_doc_vec;
    if ($self->{_idf_filter_option}) {
        foreach (keys %document_vector) {        
            $normalized_doc_vec{$_} = $document_vector{$_}
                                      *
                                      $self->{_idf_t}->{$_}
                                      /
                                      ($total_words_in_doc + 1);
        }
    }
    
    #BEGIN Construct query's concept vector
#    print Dumper(\%normalized_doc_vec); die;
    my %concept_doc_vec;
    my $concept_temp;
    my $term_temp;
    foreach $concept_temp (sort {$a <=> $b} keys $self->{_vocab_concept}) {
    	$concept_doc_vec{$concept_temp} = 0;
    	foreach $term_temp (@{$self->{_vocab_concept}->{$concept_temp}}) {
    		$concept_doc_vec{$concept_temp} += $normalized_doc_vec{$term_temp};
    	}
    }
#	print Dumper(\%concept_doc_vec);die;    
    #END Construct query's concept vector
    
    #BEGIN Remove term and concept to generate TERM_HISTOGRAMS & CONCEPT_HISTOGRAMS
    my @normalized_doc_vec_without_term;
    my @concept_doc_vec_without_concept;
    my $iCount = 0;
    
    foreach(sort keys $self->{_vocab_hist}){
    	push(@normalized_doc_vec_without_term, $normalized_doc_vec{$_});
    	$iCount++;
    	last if $iCount == $self->{_defined_vocab_size};
    }
    
    $iCount = 0;
    foreach(sort {$a <=> $b} keys $self->{_vocab_concept}) {
    	push @concept_doc_vec_without_concept, $concept_doc_vec{$_};
    	$iCount++;
    	last if $iCount == $self->{_defined_vocab_concept_size};
    }
    
#    print Dumper(\@normalized_doc_vec_without_term);
#    print Dumper(\@concept_doc_vec_without_concept);
    #END Remove term and concept to generate TERM_HISTOGRAMS & CONCEPT_HISTOGRAMS
    
    my $pwd = cwd;
    $pwd =~ m{$self->{_query_dir}.?(\S*)$};
    my $file_path_name;
    unless ( $1 eq "" ) {
        $file_path_name = "$1/$file";
    } else {
        $file_path_name = $file;
    }
    #$self->{_corpus_doc_vectors}->{$file_path_name} = \%document_vector;
    #$self->{_normalized_query_vecs}->{$file_path_name} = \%normalized_doc_vec;
    $self->{_normalized_query_vecs_without_term}->{$file_path_name} = \@normalized_doc_vec_without_term;
    $self->{_concept_query_vecs}->{$file_path_name} = \@concept_doc_vec_without_concept;
}

################ PCA Compression on Query Vecs
sub _PCA_compression_on_query_vecs {
	print "\n\t<DEBUG> 2nd generate PCA-compressed QUERY VECS using stored PCA features...\n";
	
	my $self = shift;
	chdir $self->{_working_directory};
	
	#Perform PCA analysis on queries' term vectors
	print "\n\t\t<DEBUG> PCA compression on term vectors...\n";
	
	$self->{_pca_term_features} = retrieve($self->{_pca_term_features_file}) unless $self->{_pca_term_features};
	
	my $pca = Statistics::PCA_mod->new;
	
	my $query_data = [];
	foreach(sort keys $self->{_normalized_query_vecs_without_term}) {
		push(@$query_data, $self->{_normalized_query_vecs_without_term}->{$_});
	}
	
	my $result = $pca->load_query_data({
		format	=>	'table',
		data	=>	$query_data,
	});
	
	$self->{_pca_term_features} = $pca->pca_compression_query_vecs($self->{_pca_term_features}, 240);
	
	my $pca_processed_query_matrix = $self->{_pca_term_features}->{pca_processed_query_matrix};
	
	my $iCount = 0;
	foreach(sort keys $self->{_normalized_query_vecs_without_term}) {
		$self->{_pca_term_query_vecs}{$_} = $pca_processed_query_matrix->[0][$iCount];
		$iCount++;
	}
	
	if($iCount ne scalar(keys $self->{_normalized_query_vecs_without_term})) {
		print "<Debug> Something wrong with mapping the pca query vectors back to corresponding queries\n\n";
	}
	
	#Free unused Resources
	$self->{_pca_term_features} = (); $pca = (); $query_data = (); $result = ();
	$pca_processed_query_matrix = (); $self->{_normalized_query_vecs_without_term} = ();
	
	print "\n\t\t<DEBUG> PCA compression on term vectors...DONE\n";
	#Perform PCA analysis on queries' concept vectors
	print "\n\t\t<DEBUG> PCA compression on concept vectors...\n";
	$self->{_pca_concept_features} = retrieve($self->{_pca_concept_features_file}) unless $self->{_pca_concept_features};
	
	$pca = Statistics::PCA_mod->new;
	
	$query_data = [];
	foreach(sort keys $self->{_concept_query_vecs}) {
		push @$query_data, $self->{_concept_query_vecs}->{$_};
	}
	
	$result = $pca->load_query_data({
		format	=> 'table',
		data	=> $query_data
	});
	
	$self->{_pca_concept_features} = $pca->pca_compression_query_vecs($self->{_pca_concept_features}, 240);
	$pca_processed_query_matrix = $self->{_pca_concept_features}->{pca_processed_query_matrix};
	
	$iCount = 0;
	foreach(sort keys $self->{_concept_query_vecs}) {
		$self->{_pca_concept_query_vecs}{$_} = $pca_processed_query_matrix->[0][$iCount];
		$iCount++;
	}
	
	if($iCount ne scalar(keys $self->{_concept_query_vecs})) {
		print "<Debug> Something wrong with mapping the pca query vectors back to corresponding queries\n\n";
	}
	
	#Free unused Resources
	$self->{_pca_concept_features} = (); $pca = (); $query_data = (); $result = ();
	$pca_processed_query_matrix = (); $self->{_concept_query_vecs} = ();
	
	print "\n\t\t<DEBUG> PCA compression on concept vectors...DONE\n";
#	print Dumper($self->{_pca_term_query_vecs}->{"1-original.txt"});print Dumper($self->{_pca_concept_query_vecs}->{"1-original.txt"});die;
}

############### SOM based retrieval for query docs
sub _SOM_based_retrieval {
	print "\n\t<DEBUG> 3rd retrieve RELEVANT DOCS for each QUERY from SOM map...\n";
	
	my $self = shift;
	my $retrieve_type = shift;
	
	chdir $self->{_working_directory};
	
	# Retrieve the SOM model from disk storage
	$self->{_SOM} = retrieve($self->{_SOM_db}) unless $self->{_SOM};
	
	# Prepare data for SOM
	my $query_data_vecs = [];
	my $query_data_docs = [];
	foreach(sort keys $self->{_pca_term_query_vecs}) {
		my $array_temp = [];
		
		push($query_data_docs, $_);
		push $array_temp, (@{$self->{_pca_term_query_vecs}->{$_}}, @{$self->{_pca_concept_query_vecs}->{$_}});
		push($query_data_vecs, $array_temp);
		
		$array_temp = ();
	}
	
	# Retrieve relevant docs for queries
	$self->{_SOM}->retrieve_docs_for_queries($query_data_vecs, $query_data_docs, $self->{_defined_no_of_retrieval}, $retrieve_type);
	
	#print Dumper($self->{_SOM}->{retrieval_association});die;
}

sub _retrieval_result_ranking {
	print "\n\t<DEBUG> 4th retrieval result ranking...\n";
	
	my $self = shift;
	my $retrieve_type = shift;
	
	chdir $self->{_working_directory};
	
	# Retrieve the pca_doc_vecs from disk
	$self->{_pca_term_doc_vecs} = retrieve($self->{_pca_term_doc_vecs_db}) unless $self->{_pca_term_doc_vecs};
	$self->{_pca_concept_doc_vecs} = retrieve($self->{_pca_concept_doc_vecs_db}) unless $self->{_pca_concept_doc_vecs};
	
	#--------------- DEFAULT RANKING
	#Rank retrieved documents in ascending order of distances between them and the queries
	if($retrieve_type eq "default") {
		my $query_term_vec_temp;
		my $query_concept_vec_temp;
		
		foreach(sort keys $self->{_SOM}->{retrieval_association}) {
			$query_term_vec_temp = $self->{_pca_term_query_vecs}->{$_};
			$query_concept_vec_temp = $self->{_pca_concept_query_vecs}->{$_};
			
			my @temp = sort {
				$a->[1] <=> $b->[1]
			} map {
				[
					$_->[0], 
					$self->{_term_significance} * AI::NeuralNet::SOM::Utils::vector_distance($query_term_vec_temp, $self->{_pca_term_doc_vecs}->{$_->[0]})
					+ $self->{_concept_significance} * AI::NeuralNet::SOM::Utils::vector_distance($query_concept_vec_temp, $self->{_pca_concept_doc_vecs}->{$_->[0]})
				]
			} @{$self->{_SOM}->{retrieval_association}->{$_}};
			
			$self->{_ranked_retrieval_association}->{$_} = \@temp;
		}
	} 
	
	#--------------- BMU Distance based Ranking
	elsif ($retrieve_type eq "bmu-dist") {
		#Implement the body here
		foreach(sort keys $self->{_SOM}->{retrieval_association}) {
			my @temp = sort {$a->[1] <=> $b->[1]} @{$self->{_SOM}->{retrieval_association}->{$_}};
			$self->{_ranked_retrieval_association}->{$_} = \@temp;
		}
	}
	
	eval {
        store( $self->{_ranked_retrieval_association}, $self->{_ranked_retrieval_association_db} );
    };
    if ($@) {
        print "Something went wrong with disk storage of document vectors: $@";
    }
	
	print Dumper($self->{_ranked_retrieval_association});
}

sub performance_eval {
	my $self = shift;
	my $retrieval_assoc = shift;
	
	$self->{_ranked_retrieval_association} = retrieve($retrieval_assoc) unless $self->{_ranked_retrieval_association};
	#print Dumper(sort keys $self->{_ranked_retrieval_association});die;
	
	my $no_retrieved_docs = my $no_relevant_corpus_docs = scalar(keys $self->{_ranked_retrieval_association});
	my $no_correctly_retrieved = 0;
	my @file1; my @file2; my $last_distance; my $file;
	
	foreach $file (keys $self->{_ranked_retrieval_association}) {
		@file1 = split /\-/, $file;
		
		foreach(@{$self->{_ranked_retrieval_association}->{$file}}) {
			@file2 = split /\-/, $_->[0];
			 
			if($file2[0] eq $file1[0]) {
				if($last_distance && $_->[1] != $last_distance) {
					$last_distance = ();
					@file2 = ();
					last;
				}
				$no_correctly_retrieved++;
				$last_distance = () if $last_distance;
				@file2 = ();
				last;
			} else {
				
				if($last_distance) {
					if($_->[1] == $last_distance) {
						@file2 = ();
						next;
					} else {
						$last_distance = ();print Dumper(\@file1);
						@file2 = ();
						last;
					}
				} else {
					$last_distance = $_->[1];
					@file2 = ();
					next;
				}
			}
		}
		$last_distance = ();
		
		@file1 = ();
	}
	
	print Dumper($no_correctly_retrieved);
	#$no_correctly_retrieved++;
	
	$self->{_system_performance}->{PRECISION} = $no_correctly_retrieved / $no_retrieved_docs;
	$self->{_system_performance}->{RECALL} = $no_correctly_retrieved / $no_relevant_corpus_docs;
	$self->{_system_performance}->{F_SCORE} = 2 * $self->{_system_performance}->{PRECISION} * $self->{_system_performance}->{RECALL} / ($self->{_system_performance}->{PRECISION} + $self->{_system_performance}->{RECALL});
	
	print Dumper($self->{_system_performance});
}

################## WordNet utilities
sub getSynsWordNet {
	my ($self, $term) = @_;
	
	my $wn = WordNet::QueryData->new(noload => 1);

	my @data_gen = $wn->querySense($term);
	
	my @data_pos;
	
	my $freq;
	
	if(@data_gen) {
		my @data_syns;
		foreach(@data_gen) {
			
			@data_pos = $wn->querySense($_);
	
			if(@data_pos) {
				
				foreach(@data_pos) {
					$freq = $_;
					if($wn->frequency($_)) {
						push @data_syns, $wn->querySense($freq, "syns");
						push @data_syns, $wn->querySense($freq, "hype");
						push @data_syns, $wn->querySense($freq, "hypo");
						#push @data_syns, $wn->querySense($freq, "sim");
					}
				}
				
			}
		}
		
		return undef if scalar(@data_syns) == 0;
		
		my @data_temp = map {grep {length($_) > 1 && /[a-zA-Z]/} split(/\#/,$_)} @data_syns;
		@data_temp = keys %{{map {$_ => 1} @data_temp}};
		return \@data_temp;
	}
	return undef;
}

#-----------------------FOR SUCCESSFULLY USE || REQUIRE
1;