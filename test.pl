use strict;
use warnings;
use Data::Dumper;
use Statistics::PCA_mod;
use Storable;
use Math::MatrixReal;
use AI::NeuralNet::SOM::Rect;
use AI::NeuralNet::FastSOM::Rect;
use Text::CSV;
use Cwd;
use File::Copy;


####### READ FROM A R-exported CSV
=pod
my $rows=[];
my $csv = Text::CSV->new({binary => 1}) or die "Cannot use CSV: ".Text::CSV->error_diag();

open my $fh, "<:encoding(utf8)", "R_center_50.csv" or die "R_center_50.csv: $!";
my $row = $csv->getline($fh);
#my $pointer;
while($row = $csv->getline($fh)) {
	#$pointer = [@$row[1..5]];
	
	push @$rows, $row;
}
$csv->eof() or $csv->error_diag();
close $fh;

print Dumper($rows);
=cut

###### WRITE TO R-imported CSV
=pod
my $num_docs = "_Intelligent_400";

my $term_doc_vecs = retrieve("normalized_doc_vecs_without_term_db" . $num_docs);
my $concept_doc_vecs = retrieve("concept_doc_vecs_db" . $num_docs);

#print Dumper($term_doc_vecs);
#print Dumper($concept_doc_vecs);

my $iCount = 1;
my @headers;

my $headNum;

# Export term doc vecs to CSV
foreach(keys $term_doc_vecs) {
	$headNum = scalar(@{$term_doc_vecs->{$_}});
	last if $iCount == 1;
}

while($iCount <= $headNum) {
	push @headers, $iCount;
	$iCount++;
}

my $csv = Text::CSV->new({binary => 1})
	or die "Cannot use CSV: " . Text::CSV->error_diag();
$csv->eol("\n");

print "\n<DEBUG>Writing TERM DOC VECS to CSV format....\n";

open my $fh, ">:encoding(utf8)", "perl_term_docs" . $num_docs . ".csv" or die "new.csv: $!";

$csv->print($fh, \@headers);
foreach(sort keys $term_doc_vecs) {
	$csv->print($fh, $term_doc_vecs->{$_});
}

close $fh or die "new.csv: $!";

print "\n<DEBUG>Writing TERM DOC VECS to CSV format....DONE\n";

# Export concept doc vecs to CSV
$iCount = 1;
@headers = ();

foreach(keys $concept_doc_vecs) {
	$headNum = scalar(@{$concept_doc_vecs->{$_}});
	last if $iCount == 1;
}

while($iCount <= $headNum) {
	push @headers, $iCount;
	$iCount++;
}

$csv = Text::CSV->new({binary => 1})
	or die "Cannot use CSV: " . Text::CSV->error_diag();
$csv->eol("\n");

print "\n<DEBUG>Writing CONCEPT DOC VECS to CSV format....\n";

open $fh, ">:encoding(utf8)", "perl_concept_docs" . $num_docs . ".csv" or die "new.csv: $!";

$csv->print($fh, \@headers);
foreach(sort keys $concept_doc_vecs) {
	$csv->print($fh, $concept_doc_vecs->{$_});
}

close $fh or die "new.csv: $!";

print "\n<DEBUG>Writing CONCEPT DOC VECS to CSV format....DONE\n";

=cut
####### LOOK UP WORDS on Wikipedia
=pod
#use WWW::Wikipedia;

#my $wiki = WWW::Wikipedia->new();

#my $result = $wiki->search('solution');

=pod
if($result->text()) {
	print $result->text();
}
=cut

#print join("\n", $result->related());

#print Dumper($result->categories());

###### Using WordNet
=pod
use WordNet::QueryData;

my $wn = WordNet::QueryData->new(noload => 1);

my @data_gen = $wn->querySense("absent");

my @data_pos;

my $freq;

if(@data_gen) {
	my @data_syns;
	foreach(@data_gen) {
		
		@data_pos = $wn->querySense($_);

		if(@data_pos) {
			
			foreach(@data_pos) {
				$freq = $_;
				if($wn->frequency($_)) {
					push @data_syns, $wn->querySense($freq, "syns");
					push @data_syns, $wn->querySense($freq, "hype");
					push @data_syns, $wn->querySense($freq, "hypo");
					push @data_syns, $wn->querySense($freq, "sim");
				}
			}
			
		}
	}
	
	my @data_temp = map {grep {length($_) > 1 && /[a-zA-Z]/} split(/\#/,$_)} @data_syns;
	@data_temp = keys %{{map {$_ => 1} @data_temp}};
	my $result->{absent} = \@data_temp;
	print Dumper($result);
}
=cut

####### RECREATE the PCA_FEATURES_FILE & PCA_DOC_VECS_DB from R exported file

my $numDocs = "_Intelligent_400";
my $vocab_type = "_concept";

my $R_center_file = "R" . $vocab_type . "_center" . $numDocs . ".csv";
my $R_pca_rotation_file = "R" . $vocab_type . "_pca_rotation" . $numDocs . ".csv";
my $R_pca_doc_vecs_file = "R" . $vocab_type . "_pca_doc_vecs" . $numDocs . ".csv";

my $normalized_doc_vecs_without_term_db = "normalized_doc_vecs_without_term_db" . $numDocs;
my $pca_doc_vecs_db = "pca" . $vocab_type . "_doc_vecs_db" . $numDocs;
my $pca_features_file = "pca" . $vocab_type . "_features_file" . $numDocs; 

my $iCount = 0;
my $pca_features;
my $pca_doc_vecs;

# Recreate the AVERAGES value
print "\n<DEBUG> Reading R-exported Center features...\n";

my $csv = Text::CSV->new({binary => 1}) or die "Cannot use CSV: ".Text::CSV->error_diag();

open my $fh, "<:encoding(utf8)", $R_center_file or die "$R_center_file: $!";
	my $row = $csv->getline($fh); # Get rid of the headers
	$row = $csv->getline($fh); # Read all average value in the 2nd line
close $fh;

foreach(@$row) {
	$pca_features->{averages}[$iCount]->{average} = $_;
	$iCount++;
}

print "\n<DEBUG> Reading R-exported Center features...DONE\n";

# Recreate the EIGEN_MATRIX
print "\n<DEBUG> Reading R-exported PCA Rotation matrix features...\n";

$csv = Text::CSV->new({binary => 1}) or die "Cannot use CSV: ".Text::CSV->error_diag();

open $fh, "<:encoding(utf8)", $R_pca_rotation_file or die "$R_pca_rotation_file: $!";
	$row = $csv->getline($fh); # Get rid of the headers
	# Read each line of PCA vectors
	while($row = $csv->getline($fh)) {
		push @{$pca_features->{eigen_matrix}}, $row;
	}
	
	$csv->eof() or $csv->error_diag();
close $fh;

print "\n<DEBUG> Reading R-exported PCA Rotation matrix features...DONE\n";

# Recreate the PCA_DOC_VECS
print "\n<DEBUG> Reading R-exported PCA Doc Vec features...\n";

my $normalized_doc_vecs_without_term = retrieve($normalized_doc_vecs_without_term_db);

open $fh, "<:encoding(utf8)", $R_pca_doc_vecs_file or die "$R_pca_doc_vecs_file: $!";
	$row = $csv->getline($fh); # Get rid of the headers
	# Read each line of PCA vectors
	foreach(sort keys $normalized_doc_vecs_without_term) {
		$pca_doc_vecs->{$_} = $csv->getline($fh);
	}
	
	$csv->eof() or $csv->error_diag();
close $fh;

print "\n<DEBUG> Reading R-exported PCA Doc Vec features...DONE\n";

# Store to PCA features to file
eval {
	print "\n\t<DEBUG> Storing PCA features...\n";
	store($pca_features, $pca_features_file);
	print "\n\t<DEBUG> Storing PCA features...DONE\n";
	print "\n\t<DEBUG> Storing PCA Doc Vecs...\n";
	store($pca_doc_vecs, $pca_doc_vecs_db);
	print "\n\t<DEBUG> Storing PCA Doc Vecs...DONE\n";
};
if ($@) {
	print "\n\n <Debug> Something wrong with the storage of PCA features";
}
=cut

################## LITERAL & INTELLIGENT PLAGIARISM FILE FILTERING
=pod
my $dir = "Webis-CPC-11";
my $root = cwd;

chdir $dir or die "Unable to change directory to $dir: $!";

$dir = cwd;
my @brokeup;
my @clean_words;
my $file;
my $file1;

my $iCount = 0;
foreach(glob "*metadata.txt") {
	$file = $_;#print Dumper($_);die if $iCount == 20; $iCount++;
	if(-d and !(-l)) {
		chdir $dir or die "Unable to change directory to $dir: $!";
	} elsif(-r _ and -T _ and -M _ > 0.00001 and !(-l $_) and !m{\.ps$} and !m{\.pdf$} and !m{\.eps$} and !m{\.out$} and !m{~$}) {
		unless (open IN, $_) {
	        die "Unable to open file $_ in the corpus: $!\n";		        
	    }
	    
	    while (<IN>) {
	    	chomp;
	       	@brokeup = split /\:|\s+/, $_;
	       	@clean_words = grep $_, map { /([a-z0-9_]{2,})/i;$1 } @brokeup;
	    }
	    close IN;
	    
	    if($clean_words[1] eq "No") {
	    	print "<DEBUG> moving $file...\n";
	    	
	    	@brokeup = split /\-/, $file;
	    	$file = $brokeup[0] . "-original.txt";
	    	$file1 = $brokeup[0] . "-paraphrase.txt";
	    	
	    	chdir $root or die "Unable to change directory to $root: $!";
	    	
	    	move("Webis-CPC-11/" . $file, "Original_Literal");
	    	move("Webis-CPC-11/" . $file1, "Paraphrase_Literal");
	    	
	    	chdir $dir or die "Unable to change directory to $dir: $!";
	    }
	}
}
=cut