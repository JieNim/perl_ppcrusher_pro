use strict;
use warnings;
use STM;
use Data::Dumper;

my $corpus_size = "_Intelligent_400";

# For vocabulary & word histograms construction
my $corpus_dir = "Original" . $corpus_size;
#my $corpus_dir = "Candidate";

my $stop_words_file = "stop_words_l.txt";
my $corpus_vocab_db = "corpus_vocab_db" . $corpus_size;
my $vocab_concept_db = "vocab_concept" . $corpus_size;

my $idf_t_db = "idf_t_db" . $corpus_size;

my $normalized_doc_vecs_without_term_db = "normalized_doc_vecs_without_term_db" . $corpus_size;
my $concept_doc_vecs_db = "concept_doc_vecs_db" . $corpus_size;

# For PCA
my $pca_concept_doc_vecs_db = "pca_concept_doc_vecs_db" . $corpus_size;
my $pca_concept_features_file = "pca_concept_features_file" . $corpus_size;;
my $pca_term_doc_vecs_db = "pca_term_doc_vecs_db" . $corpus_size;
my $pca_term_features_file = "pca_term_features_file" . $corpus_size;

# For SOM
my $SOM_db = "SOM_db" . $corpus_size;

# For Retrieval
my $query_dir = "Paraphrase" . $corpus_size;
#my $query_dir = "Query";
my $ranked_retrieval_association_db = "ranked_retrieval_association" . $corpus_size;

################# VOCABULARY & HISTOGRAMS CONSTRUCTION
my $stm = STM->new(
	concept_doc_vecs_db	=> $concept_doc_vecs_db,
	corpus_directory	=> $corpus_dir,
	corpus_vocab_db		=> $corpus_vocab_db,
	idf_t_db			=> $idf_t_db,
	normalized_doc_vecs_without_term_db => $normalized_doc_vecs_without_term_db,
	pca_concept_doc_vecs_db		=> $pca_concept_doc_vecs_db,
	pca_concept_features_file	=> $pca_concept_features_file,
	pca_term_doc_vecs_db		=> $pca_term_doc_vecs_db,
	pca_term_features_file	=> $pca_term_features_file,
	ranked_retrieval_association_db => $ranked_retrieval_association_db,
	SOM_db				=> $SOM_db,
	stop_words_file		=> $stop_words_file,
	query_directory		=> $query_dir,
	vocab_concept_db	=> $vocab_concept_db,
	want_stemming		=> 1
);

#$stm->construct_corpus_vocabulary();
$stm->upload_stm_model_from_disk();

#$stm->display_corpus_vocab("concept");

#$stm->generate_document_vectors();

#$stm->dump_normalized_doc_vectors();

################ PCA COMPRESSION <== USE R-Script INSTEAD
#$stm->PCAProjectionMatrixConstruction();

################ SOM CLUSTERING
#$stm->SOMClustering();

################ DOCUMENT RETRIEVAL
#$stm->docs_retrieval();

$stm->performance_eval($ranked_retrieval_association_db);
#print Dumper($stm);

print "<DEBUG>End main\n";